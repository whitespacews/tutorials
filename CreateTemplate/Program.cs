﻿using System;
using AutoMapper;
using Common;
using Common.Logging;
using Common.Service.AOM;
using CreateTemplate.Models;
using CreateTemplate.Models.Enum;
using CreateTemplate.Models.Fields;
using CreateTemplate.ObjectMaps;

namespace CreateTemplate
{
    class Program
    {
        static void Main(string[] args)
        {
            //  Initialise mappings.  Allows objects
            //  to be changed from our custom types
            //  over to AOM API types more easily.
            Mapper.Initialize(config =>
            {
                //  Converts the template object to
                //  CreateJobSoapRequest object.
                config.AddProfile<TemplateProfile>();

                //  Converts the custom field types to
                //  AOM API JobSoapField objects.
                config.AddProfile<DividerProfile>();
                config.AddProfile<TextBoxProfile>();
                config.AddProfile<DropDownListProfile>();
            });

            Template template = SetupTemplate();

            //  Finished with the template object, 
            //  build the request object using
            //  AutoMapper.
            CreateJobRequest req = Mapper.Map<CreateJobRequest>(template);
            req.ApplicationReference = "test";

            //  Execute the SOAP operation.
            CreateJobsResponse resp = AOM.Execute(x => x.CreateJobs(new CreateJobsRequest
            {
                CreateJobs = new CreateJobRequest[]
                {
                    req
                }
            }));

            if (!resp.Success)
                Logger.Current.Error(resp.Error);
            else
            {
                foreach (var inner in resp.Responses)
                {
                    if (inner.Success)
                    {
                        Logger.Current.Info($"Template [{inner.JobId}] successfully created.");
                    }
                    else
                    {
                        Logger.Current.Error(inner.Error);
                    }
                }
            }

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }

        private static Template SetupTemplate()
        {
            //  Setup a basic template
            Template template = new Template
            {
                Title = "Example Template"
            };

            //  Brief description of the work to be carried out.
            var divider = (Divider)JobFieldFactory.CreateNew(JobFieldType.Divider, "description");
            divider.Text = "";
            template.AddField(divider);

            //  Enter the drivers name.
            var textbox = (TextBox)JobFieldFactory.CreateNew(JobFieldType.TextBox, "driver_name");
            textbox.DisplayName = "Driver name";
            textbox.Value = "John Doe";
            textbox.IsReadOnly = true;
            template.AddField(textbox);

            //  Select the vehicle registration.
            var dropdown = (DropDownList) JobFieldFactory.CreateNew(JobFieldType.DropDownList, "vehicle_registration");
            dropdown.DisplayName = "Vehicle Registration";
            dropdown.AddItem("AA11 AAA");
            dropdown.AddItem("BB22 BBB");
            dropdown.AddItem("CC33 CCC");
            dropdown.SelectedItem = "CC33 CCC";
            template.AddField(dropdown);

            //  Update the divider by finding it by its name, 
            //  turns out the description wasn't set.
            divider = (Divider)template["description"];
            divider.Text = "Please pickup the following vehicle:";

            return template;
        }
    }
}
