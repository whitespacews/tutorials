﻿using Common.Service.AOM;
using CreateTemplate.Models;
using CreateTemplate.Models.Fields;

namespace CreateTemplate.ObjectMaps
{
    public class DividerProfile : AutoMapper.Profile
    {
        public DividerProfile()
        {
            CreateMap<Divider, CreateJobSoapField>()
                .ForMember(d => d.FieldType, opt => opt.UseValue("Divider"))
                .ForMember(d => d.DisplayName, opt => opt.MapFrom(m => m.Text))
                ;
        }
    }
}