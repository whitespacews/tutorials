﻿using System.Linq;
using System.Text;
using AutoMapper;
using Common.Service.AOM;
using CreateTemplate.Models.Fields;

namespace CreateTemplate.ObjectMaps
{
    public class DropDownListParamsResolver : IValueResolver<DropDownList, string>
    {
        public string Resolve(DropDownList source, string destination, ResolutionContext context)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"VALUES:{string.Join(",", source.Items)};");

            return sb.ToString();
        }
    }

    public class DropDownListProfile : Profile
    {
        public DropDownListProfile()
        {
            CreateMap<DropDownList, CreateJobSoapField>()
                .ForMember(d => d.FieldType, opt => opt.UseValue("DropDownList"))
                .ForMember(d => d.DisplayName, opt => opt.MapFrom(m => m.DisplayName))
                .ForMember(d => d.Parameters, opt => opt.ResolveUsing<DropDownListParamsResolver>())
                .ForMember(d => d.Data, opt => opt.MapFrom(m => m.SelectedItem))
                ;
        }
    }
}