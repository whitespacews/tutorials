﻿using Common.Service.AOM;
using CreateTemplate.Models;
using CreateTemplate.Models.Fields.Base;

namespace CreateTemplate.ObjectMaps
{
    public class TemplateProfile : AutoMapper.Profile
    {
        public TemplateProfile()
        {
            CreateMap<Template, CreateJobRequest>()
                .ForMember(d => d.Job, opt => opt.MapFrom(x => x))
                ;

            CreateMap<Template, CreateJobSoap>()
                .ForMember(d => d.Title, opt => opt.MapFrom(m => m.Title))
                .ForMember(d => d.Reference, opt => opt.UseValue(string.Empty))
                .ForMember(d => d.Fields, opt => opt.MapFrom(m => m.Fields))
                ;
        }
    }
}