﻿using System;
using System.Text;
using AutoMapper;
using Common.Service.AOM;
using CreateTemplate.Models;
using CreateTemplate.Models.Enum;
using CreateTemplate.Models.Fields;

namespace CreateTemplate.ObjectMaps
{
    public class TextBoxOptionsResolver : IValueResolver<TextBox, string>
    {
        public string Resolve(TextBox source, string destination, ResolutionContext context)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{nameof(KeyboardType)}:{source.KeyboardType}");
            
            return sb.ToString();
        }
    }

    public class TextBoxParamsResolver : IValueResolver<TextBox, string>
    {
        public string Resolve(TextBox source, string destination, ResolutionContext context)
        {
            StringBuilder sb = new StringBuilder();

            if (source.IsReadOnly)
                source.MaxLength = 0;

            sb.Append($"LENGTH:{source.MaxLength};");

            return sb.ToString();
        }
    }

    public class TextBoxProfile : AutoMapper.Profile
    {
        public TextBoxProfile()
        {
            CreateMap<TextBox, CreateJobSoapField>()
                .ForMember(d => d.FieldType, opt => opt.UseValue("TextBox"))
                .ForMember(d => d.DisplayName, opt => opt.MapFrom(m => m.DisplayName))
                .ForMember(d => d.Options, opt => opt.ResolveUsing<TextBoxOptionsResolver>())
                .ForMember(d => d.Parameters, opt => opt.ResolveUsing<TextBoxParamsResolver>())
                .ForMember(d => d.Data, opt => opt.MapFrom(m => m.Value))
                ;
        }
    }
}