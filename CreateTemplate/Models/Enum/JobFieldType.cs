namespace CreateTemplate.Models.Enum
{
    public enum JobFieldType
    {
        None = 0,
        TextBox,
        Divider,
        DropDownList
    }
}