namespace CreateTemplate.Models.Enum
{
    public enum KeyboardType
    {
        Standard = 0,
        StandardNumeric,
        Url,
        Numeric
    }
}