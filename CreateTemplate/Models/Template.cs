﻿using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using CreateTemplate.Models.Fields.Base;

namespace CreateTemplate.Models
{
    public class Template
    {
        private readonly Dictionary<string, JobField> _fields = new Dictionary<string, JobField>(1000);

        public string Title { get; set; }
        public JobField[] Fields => _fields.Values.ToArray();

        public JobField this[string fieldName] => _fields.ContainsKey(fieldName) ? _fields[fieldName] : null;

        public void AddField(JobField field)
        {
            if (_fields.ContainsKey(field.Name))
            {
                Logger.Current.Warn($"Field already exists with matching key [{field.Name}]");
                return;
            }

            _fields.Add(field.Name, field);
        }

        public void RemoveField(string fieldName)
        {
            if (_fields.ContainsKey(fieldName))
                _fields.Remove(fieldName);
        }
    }
}