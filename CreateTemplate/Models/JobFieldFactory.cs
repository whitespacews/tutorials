using System;
using CreateTemplate.Models.Enum;
using CreateTemplate.Models.Fields;
using CreateTemplate.Models.Fields.Base;

namespace CreateTemplate.Models
{
    public static class JobFieldFactory
    {
        public static JobField CreateNew(JobFieldType fieldType, string fieldName)
        {
            switch (fieldType)
            {
                case JobFieldType.TextBox:
                    return new TextBox(fieldName);
                case JobFieldType.Divider:
                    return new Divider(fieldName);
                case JobFieldType.DropDownList:
                    return new DropDownList(fieldName);
                default:
                    throw new ArgumentOutOfRangeException(nameof(fieldType), fieldType, null);
            }
        }
    }
}