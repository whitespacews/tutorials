using CreateTemplate.Models.Enum;

namespace CreateTemplate.Models.Fields.Base
{
    public abstract class JobField
    {
        protected JobField(string name, JobFieldType fieldType)
        {
            Name = name;
            FieldType = fieldType;
        }

        public string Name { get; protected set; }
        public JobFieldType FieldType { get; protected set; }
    }
}