using CreateTemplate.Models.Enum;
using CreateTemplate.Models.Fields.Base;

namespace CreateTemplate.Models.Fields
{
    public class Divider : JobField
    {
        public Divider(string name) : base(name, JobFieldType.Divider)
        {
        }

        public string Text { get; set; }
    }
}