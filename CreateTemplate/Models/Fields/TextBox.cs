using CreateTemplate.Models.Enum;
using CreateTemplate.Models.Fields.Base;

namespace CreateTemplate.Models.Fields
{
    public class TextBox : JobField
    {
        public TextBox(string name) : base(name, JobFieldType.TextBox)
        {
        }

        public bool IsReadOnly { get; set; }
        public int MaxLength { get; set; }
        public string Value { get; set; }
        public string DisplayName { get; set; }
        public KeyboardType KeyboardType { get; set; }
    }
}