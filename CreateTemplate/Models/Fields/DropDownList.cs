using System.Collections.Generic;
using CreateTemplate.Models.Enum;
using CreateTemplate.Models.Fields.Base;

namespace CreateTemplate.Models.Fields
{
    public class DropDownList : JobField
    {
        private readonly List<string> _items = new List<string>(50);
        private string _selectedItem;

        public DropDownList(string name) : base(name, JobFieldType.DropDownList)
        {
        }

        public string[] Items => _items.ToArray();

        public string SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value.Trim(); }
        }

        public string DisplayName { get; set; }

        public void AddItem(string item)
        {
            item = item.Trim();

            if (!_items.Contains(item))
                _items.Add(item);
        }
    }
}