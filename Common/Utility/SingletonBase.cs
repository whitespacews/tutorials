using System;
using System.Diagnostics;

namespace Common.Utility
{
    public abstract class SingletonBase<T> where T : class
    {
        private static readonly Lazy<T> _instance = new Lazy<T>(() =>
        {
            var instance = (T)Activator.CreateInstance(typeof(T), true);
            var initializable = instance as IInitialisable;
            initializable?.Initialise();

            return instance;
        });

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        [DebuggerNonUserCode]
        public static T Current => _instance.Value;
    }

    public interface IInitialisable
    {
        void Initialise();
    }
}