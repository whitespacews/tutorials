﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using Common.Logging;
using Common.Service.AOM;

namespace Common
{
    public static class AOM
    {
        const string API_USERNAME = "api_username";
        const string API_PASSWORD = "api_password";

        public static TResult Execute<TResult>(Func<IApiService, TResult> func)
        {
            Logger.Current.Debug($"Starting comms with AOM...");
            Stopwatch sw = Stopwatch.StartNew();

            ApiServiceClient client = new ApiServiceClient();
            client.ClientCredentials.UserName.UserName = AOM.API_USERNAME;
            client.ClientCredentials.UserName.Password = AOM.API_PASSWORD;

            try
            {
                Logger.Current.Debug($" > Sending request");
                TResult result = func(client);
                sw.Stop();

                Logger.Current.Debug($" > Received response [{func.Method.ReturnType.Name}] after {sw.Elapsed}.");

                client.Close();

                return result;
            }
            catch (Exception ex)
            {
                client.Abort();
                Logger.Current.Error("Failed to communicate with AllOnMobile.", ex);
                throw;
            }
        }
    }
}