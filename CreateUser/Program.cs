﻿using System;
using System.Text;
using Common;
using Common.Logging;
using Common.Service.AOM;

namespace CreateUser
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var req = new CreateUserAccountRequest
            {
                //  Unique username for the account.  Once an
                //  account is in use it cannot be used again
                //  without deleting the user prior.
                Username = "example@allonmobile.com",

                //  Randomly generate a 6 character password.
                Password = GeneratePassword(6),

                //  Give access to the AllOnMobile Bridge.
                IsAdministrator = true
            };

            //  Execute the SOAP operation.
            CreateUserAccountResponse resp = AOM.Execute(x => x.CreateUserAccount(req));

            if (!resp.Success)
                Logger.Current.Error(resp.Error);
            else
                Logger.Current.Info("User account successfully created.");

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }

        private static string GeneratePassword(int length)
        {
            Logger.Current.Debug("Generating password...");

            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            StringBuilder sb = new StringBuilder();
            Random rnd = new Random();

            while (0 < length--)
                sb.Append(valid[rnd.Next(valid.Length)]);

            string result = sb.ToString();

            Logger.Current.Info($" > {result}");
            return result;
        }
    }
}