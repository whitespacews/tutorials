﻿using System;
using Common;
using Common.Logging;
using Common.Service.AOM;

namespace UpdateUser
{
    class Program
    {
        static void Main(string[] args)
        {
            var req = new UpdateUserAccountRequest
            {
                //  Used for finding the user.
                Username = "example@allonmobile.com",

                //  Must be provided if the account holds administrator privileges otherwise it would default to false.
                IsAdministrator = true,

                //  Change the account's friendly display to 'Example Account'.
                DisplayName = "Example Account"
            };

            //  Execute the SOAP operation.
            UpdateUserAccountResponse resp = AOM.Execute(x => x.UpdateUserAccount(req));

            if (!resp.Success)
                Logger.Current.Error(resp.Error);
            else
                Logger.Current.Info("User account successfully changed.");

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }
    }
}