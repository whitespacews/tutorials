# ALLONMOBILE CODE EXAMPLES #

AllOnMobile Ltd makes every effort to ensure that code listed here is accurate and free from error. However, caution is required when using any of the code provided by AllOnMobile.
 
Customers use the code provided at their own risk and AllOnMobile accepts no responsibility for any loss caused by using the code examples provided.